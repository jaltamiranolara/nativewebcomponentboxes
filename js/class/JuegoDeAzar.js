export default class JuegoDeAzar extends HTMLElement {
    constructor(){
        super()
        this.shadow = this.attachShadow({mode: 'open'})
    }
    connectedCallback() {
        this.initJuego()
    }

    attributeChangedCallback(name, oldValue, newValue) {
        if(oldValue){
            this.reset()
            this.initJuego()
        }
    }
    static get observedAttributes() {
        return ['boxes'];
    }

    initJuego(){
        this.randomNumber = Math.floor(Math.random() * (this.boxes - 1) + 1)

        let div, link = document.createElement('link')
        link.setAttribute('rel', 'stylesheet')
        link.setAttribute('type', 'text/css')
        link.setAttribute('href', './js/class/JuegoDeAzar.css')
        this.shadow.appendChild(link)
        this.style.setProperty('display', 'flex')
        this.style.setProperty('flex-wrap', 'wrap')

        for (let index = 1; index <= this.boxes; index++) {
            div = document.createElement('div')
            div.textContent = index
            div.onclick = event => this.handleBoxClick(event)
            this.shadow.appendChild(div)
        }
    }
    handleBoxClick (event) {
        if(!isNaN(event.srcElement.outerText)){
            if(parseInt(event.srcElement.outerText) === this.randomNumber){
                event.srcElement.textContent = 'OK'
                event.srcElement.setAttribute('class', 'win')
                setTimeout(() => this.boxes = parseInt(prompt('¿Cuantas cajas?')) || 3, 250)
            } else {
                event.srcElement.textContent = 'X'
                event.srcElement.setAttribute('class', 'fail')
            }
        }
    }
    reset (){
        this.shadow.innerHTML = ''
    }
    get boxes (){
        return parseInt(this.getAttribute('boxes'))
    }
    
    set boxes (newValue) {
        this.setAttribute('boxes', newValue)
    }

}